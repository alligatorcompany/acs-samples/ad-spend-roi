# Snowpark
from snowflake.snowpark.session import Session
from snowflake.snowpark.version import VERSION
# Misc
import json


def sf_connect_json_params():
    """Connect to Snowflake using connection.json."""
    connection_parameters = json.load(open('../utils/connection.json'))

    try:
        # Create Snowflake Session object
        session = Session.builder.configs(connection_parameters).create()
        print("Connected!")
        # Environment details
        session.sql_simplifier_enabled = True
        snowflake_environment = session.sql('select current_user(), current_version()').collect()
        snowpark_version = VERSION
        print("Environment details:")
        print(f"User                        : {snowflake_environment[0][0]}")
        print(f"Role                        : {session.get_current_role()}")
        print(f"Database                    : {session.get_current_database()}")
        print(f"Schema                      : {session.get_current_schema()}")
        print(f"Warehouse                   : {session.get_current_warehouse()}")
        print(f"Snowflake version           : {snowflake_environment[0][1]}")
        print(f"Snowpark for Python version : {snowpark_version[0]}.{snowpark_version[1]}.{snowpark_version[2]}")
    except Exception as e:
        print(f"Error: {e}")

    return session
