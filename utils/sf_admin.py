def sf_show_df_history(session, df, rows):
    """Send the DF SQL for execution on the server.
    Get query history for a dataframe: return queries from History object.
    """
    with session.query_history() as history:
        df.show(rows)

    return history.queries
