## Project Overview
Perform data analysis and data preparation tasks to train a Linear Regression model to predict future ROI (Return On Investment) of variable ad spend budgets across multiple 
channels including search, video, social media, and email using Snowpark for Python, Snowpark ML, and Streamlit. 

### Deliverable
An interactive web application deployed visualizing the ROI of different allocated advertising spend budgets:
[SkiGear Co Ad Spend Optimizer](https://app.snowflake.com/jnsiivu/alligator/#/streamlit-apps/DEV_DONI.DASH_SCHEMA.D1LR7SRF6OA_PWHP)

## Tasks
### Data Engineering
Data Engineering in Snowflake using Snowpark for Python:

* Load data from Snowflake tables into Snowpark DataFrames
* Perform Exploratory Data Analysis on Snowpark DataFrames
* Pivot and Join data from multiple tables using Snowpark DataFrames
* Automate data preparation using Snowflake Tasks

### Machine Learning
Machine Learning in Snowflake using Snowpark for Python:

* Load features and target from Snowflake table into Snowpark DataFrame
* Prepare features for model training
* Train ML model using Snowpark ML in Snowflake and upload the model to Snowflake stage
* Register ML model and use it for inference from Snowpark ML Model Registry
